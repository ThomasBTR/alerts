package com.safetynet.alerts.server.services;

import com.safetynet.alerts.server.constants.EActionsProceedConstants;
import com.safetynet.alerts.server.constants.EObjectConstants;
import com.safetynet.alerts.server.constants.EStatusConstants;
import com.safetynet.alerts.server.database.entities.AllergeneEntity;
import com.safetynet.alerts.server.database.entities.MedicationEntity;
import com.safetynet.alerts.server.database.entities.MedicineEntity;
import com.safetynet.alerts.server.database.entities.PersonEntity;
import com.safetynet.alerts.server.database.repositories.AllergeneRepository;
import com.safetynet.alerts.server.database.repositories.MedicationRepository;
import com.safetynet.alerts.server.database.repositories.PersonRepository;
import com.safetynet.alerts.server.mapping.IPersonMapper;
import io.swagger.model.MedicalRecord;
import io.swagger.model.Medicalrecords;
import io.swagger.model.PersonRsp;
import io.swagger.model.PersonsRsp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Medication post services.
 */
@Service
public class MedicationPostServices {

	/**
	 * The Person repository.
	 */
	@Autowired
	public PersonRepository personRepository;

	/**
	 * The Medication repository.
	 */
	@Autowired
	public MedicationRepository medicationRepository;

	/**
	 * The Allergene repository.
	 */
	@Autowired
	public AllergeneRepository allergeneRepository;

	private int medicationId = 0;
	private int medicineId = 0;
	private int allergieId = 0;

	/**
	 * Instantiates a new Medication post services.
	 *
	 * @param personRepository     the person repository
	 * @param medicationRepository the medication repository
	 * @param allergeneRepository  the allergene repository
	 */
	public MedicationPostServices(final PersonRepository personRepository, final MedicationRepository medicationRepository, final AllergeneRepository allergeneRepository) {
		this.medicationRepository = medicationRepository;
		this.allergeneRepository = allergeneRepository;
		this.personRepository = personRepository;
	}

	/**
	 * MedicationPostServices logger.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(MedicationPostServices.class);

	/**
	 * Add medical records persons rsp.
	 *
	 * @param body the body
	 * @return the persons rsp
	 */
	public PersonsRsp addMedicalRecords(final Medicalrecords body) {
		LOGGER.debug(EActionsProceedConstants.ADDING_MULTIPLE_MEDICAL_RECORDS_START.getValue());
		try {
			PersonsRsp personRspList = new PersonsRsp();
			for (MedicalRecord medicalrecord
					:
					body.getMedicalrecords()) {
				PersonEntity person = personRepository.findPersonEntityByNameEntityLike(medicalrecord.getFirstName(), medicalrecord.getLastName());
				LOGGER.debug(EStatusConstants.DATA_RECEIVED_SOLO.getValue(), EObjectConstants.PERSON);
				person.setBirthdate(getBirthDate(medicalrecord.getBirthdate()));

				List<MedicationEntity> medicationEntities = getMedicationsEntities(medicalrecord, person);
				medicationRepository.saveAll(medicationEntities);
				person.setMedications(medicationEntities);

				List<AllergeneEntity> allergeneEntities = getAllergies(medicalrecord);
				allergeneRepository.saveAll(allergeneEntities);
				person.setAllergies(allergeneEntities);


				personRepository.save(person);

				PersonRsp personRsp = IPersonMapper.INSTANCE.personEntityWithMedicationToPersonRsp(person);

				personRspList.addPersonsItem(personRsp);
			}
			return personRspList;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e.getStackTrace(), e);
			return null;
		}
	}

	/**
	 * Get a formatted bithdate in a LocalDate object.
	 *
	 * @param birthdate the birthdate in string format.
	 * @return the formatted birthdate.
	 */
	private LocalDate getBirthDate(final String birthdate) {
		return LocalDate.parse(birthdate, DateTimeFormatter.ofPattern("MM/dd/yyyy"));

	}

	/**
	 * Get the allergies from a medical Record object.
	 *
	 * @param medicalrecord medical Record
	 * @return a list of Allergene Entity.
	 */
	private List<AllergeneEntity> getAllergies(final MedicalRecord medicalrecord) {
		List<AllergeneEntity> allergeneEntities = new ArrayList<>();

		for (String allergene
				:
				medicalrecord.getAllergies()) {
			AllergeneEntity allergeneEntity = new AllergeneEntity();
			allergeneEntity.setAllergene(allergene);
			allergeneEntity.setId(++allergieId);
			allergeneEntities.add(allergeneEntity);
		}

		return allergeneEntities;

	}

	/**
	 * Get Medications entities from a medical record and a person Entity.
	 *
	 * @param medicalrecord the medical record
	 * @param personEntity  the person
	 * @return a list of medication Entities.
	 */
	private List<MedicationEntity> getMedicationsEntities(final MedicalRecord medicalrecord, final PersonEntity personEntity) {
		List<MedicationEntity> medicationEntities = new ArrayList<>();
		for (String medications
				:
				medicalrecord.getMedications()) {
			MedicationEntity medicationEntity = new MedicationEntity();
			MedicineEntity medicineEntity = new MedicineEntity();

			String[] arr = medications.split(":");
			medicineEntity.setMedecineName(arr[0]);
			medicineEntity.setId(++medicineId);
			medicationEntity.setMedicineEntity(medicineEntity);

			String[] all = arr[1].split("mg");
			medicationEntity.setDosage(Integer.parseInt(all[0]));
			medicationEntity.setPersonEntity(personEntity);
			medicationEntity.setId(++medicationId);
			medicationEntities.add(medicationEntity);
		}
		return medicationEntities;
	}

	/**
	 * Add medical record.
	 *
	 * @param medicalRecord the medical record
	 */
	public void addMedicalRecord(final MedicalRecord medicalRecord) {
		LOGGER.debug(EActionsProceedConstants.ADDING_MEDICAL_RECORD_START.getValue(), medicalRecord.getFirstName(), medicalRecord.getLastName());
		PersonEntity person = personRepository.findPersonEntityByNameEntityLike(medicalRecord.getFirstName(), medicalRecord.getLastName());
		LOGGER.debug(EStatusConstants.DATA_RECEIVED_SOLO.getValue(), EObjectConstants.PERSON);
		person.setBirthdate(getBirthDate(medicalRecord.getBirthdate()));

		List<MedicationEntity> medicationEntities = getMedicationsEntities(medicalRecord, person);
		medicationEntities = medicationRepository.saveAll(medicationEntities);
		person.setMedications(medicationEntities);

		List<AllergeneEntity> allergeneEntities = getAllergies(medicalRecord);
		allergeneEntities = allergeneRepository.saveAll(allergeneEntities);
		person.setAllergies(allergeneEntities);
		personRepository.save(person);
	}

	/**
	 * Update medical record.
	 *
	 * @param firstName     the first name
	 * @param lastName      the last name
	 * @param medicalRecord the medical record
	 */
	public void updateMedicalRecord(final String firstName, final String lastName, final MedicalRecord medicalRecord) {
		LOGGER.debug(EActionsProceedConstants.UPDATING_MEDICAL_RECORD_START.getValue(), medicalRecord.getFirstName(), medicalRecord.getLastName());
		PersonEntity person = personRepository.findPersonEntityByNameEntityLike(firstName, lastName);
		LOGGER.debug(EStatusConstants.DATA_RECEIVED_SOLO.getValue(), EObjectConstants.PERSON);
		person.setBirthdate(getBirthDate(medicalRecord.getBirthdate()));

		List<MedicationEntity> medicationEntities = getMedicationsEntities(medicalRecord, person);
		medicationEntities = medicationRepository.saveAll(medicationEntities);
		person.setMedications(medicationEntities);

		List<AllergeneEntity> allergeneEntities = getAllergies(medicalRecord);
		allergeneEntities = allergeneRepository.saveAll(allergeneEntities);
		person.setAllergies(allergeneEntities);
		personRepository.save(person);
	}

	/**
	 * Delete medical record.
	 *
	 * @param firstName the first name
	 * @param lastName  the last name
	 */
	public void deleteMedicalRecord(final String firstName, final String lastName) {
		LOGGER.debug(EActionsProceedConstants.DELETING_MEDICAL_RECORD_START.getValue(), firstName, lastName);
		PersonEntity person = personRepository.findPersonEntityByNameEntityLike(firstName, lastName);
		LOGGER.debug(EStatusConstants.DATA_RECEIVED_SOLO.getValue(), EObjectConstants.PERSON);
		person.setBirthdate(LocalDate.now());

		List<MedicationEntity> medicationEntities = new ArrayList<>();
		medicationEntities = medicationRepository.saveAll(medicationEntities);
		person.setMedications(medicationEntities);

		List<AllergeneEntity> allergeneEntities = new ArrayList<>();
		allergeneEntities = allergeneRepository.saveAll(allergeneEntities);
		person.setAllergies(allergeneEntities);
		personRepository.save(person);
	}
}
