package com.safetynet.alerts.server.services;

import com.safetynet.alerts.server.constants.EActionsProceedConstants;
import com.safetynet.alerts.server.constants.EObjectConstants;
import com.safetynet.alerts.server.constants.EStatusConstants;
import com.safetynet.alerts.server.database.entities.AddressEntity;
import com.safetynet.alerts.server.database.entities.PersonEntity;
import com.safetynet.alerts.server.database.repositories.PersonRepository;
import com.safetynet.alerts.server.mapping.IFirestationMapper;
import io.swagger.model.AddressRsp;
import io.swagger.model.AddressesRsp;
import io.swagger.model.Firestation1;
import io.swagger.model.Firestations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * The type Firestation post services.
 */
@Service
public class FirestationPostServices {

	/**
	 * The Person repository.
	 */
	@Autowired
	public PersonRepository personRepository;

	/**
	 * Instantiates a new Firestation post services.
	 *
	 * @param personRepository the person repository
	 */
	public FirestationPostServices(final PersonRepository personRepository) {
		this.personRepository = personRepository;
	}

	/**
	 * FirestationPostServices logger.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(FirestationPostServices.class);

	/**
	 * Add firestations addresses rsp.
	 *
	 * @param body the body
	 * @return the addresses rsp
	 */
	public AddressesRsp addFirestations(final Firestations body) {
		LOGGER.debug(EActionsProceedConstants.ADDING_MULTIPLE_FIRESTATIONS_START.getValue());
		try {
			AddressesRsp addressesRsp = new AddressesRsp();


			for (Firestation1 firestation
					:
					body.getFirestations()) {
				List<PersonEntity> personEntities = personRepository.findPersonEntityByAddressEntityEquals(firestation.getAddress());
				AddressRsp addressRsp = new AddressRsp();
				for (PersonEntity person : personEntities) {

					AddressEntity addressEntity = person.getAddressEntity();
					addressEntity.setStation(firestation.getStation());
					person.setAddressEntity(addressEntity);
					personRepository.save(person);
					AddressRsp addressbuffer = IFirestationMapper.INSTANCE.addressRspFill(addressEntity);
					if (!addressbuffer.getAddress().equals(addressRsp.getAddress())) {
						addressRsp = addressbuffer;
						addressesRsp.addAddressesItem(addressRsp);
					}
				}
			}
			return addressesRsp;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e.getStackTrace(), e);
			return null;
		}
	}

	/**
	 * Add firestation mapping to a specified address.
	 *
	 * @param stationNumber the station number
	 * @param address       the address
	 */
	public void addFirestationMappingToASpecifiedAddress(final Integer stationNumber, final String address) {
		LOGGER.debug(EActionsProceedConstants.ADDING_FIRESTATION_START.getValue(), stationNumber, address);

		List<PersonEntity> personEntities = personRepository.findPersonEntityByAddressEntityEquals(address);
		LOGGER.debug(EStatusConstants.DATA_RECEIVED_LIST.getValue(), EObjectConstants.PERSON.getObject(), personEntities.size());

		for (PersonEntity person
				:
				personEntities) {
			AddressEntity addressEntity = person.getAddressEntity();
			addressEntity.setStation(stationNumber);
			person.setAddressEntity(addressEntity);

		}

		LOGGER.info(EActionsProceedConstants.ADDING_FIRESTATION_SUCCESS.getValue(), stationNumber, address);
	}

	/**
	 * Update firestation mapping to a specified address.
	 *
	 * @param station the station
	 * @param address the address
	 */
	public void updateFirestationMappingToASpecifiedAddress(final int station, final String address) {
		LOGGER.debug(EActionsProceedConstants.UPDATING_FIRESTATION_START.getValue(), station, address);

		List<PersonEntity> personEntities = personRepository.findPersonEntityByAddressEntityEquals(address);
		LOGGER.debug(EStatusConstants.DATA_RECEIVED_LIST.getValue(), EObjectConstants.PERSON.getObject(), personEntities.size());

		for (PersonEntity person
				:
				personEntities) {
			AddressEntity addressEntity = person.getAddressEntity();
			addressEntity.setStation(station);
			person.setAddressEntity(addressEntity);
		}
	}


	/**
	 * Delete firestation mapping to a specified address.
	 *
	 * @param station the station
	 * @param address the address
	 */
	public void deleteFirestationMappingToASpecifiedAddress(final int station, final String address) {
		LOGGER.debug(EActionsProceedConstants.DELETING_FIRESTATION_START.getValue(), station, address);
		try {
			List<PersonEntity> personEntities = personRepository.findPersonEntitiesByAddressEntityStation(station);
			for (PersonEntity person
					:
					personEntities) {
				AddressEntity addressEntity = new AddressEntity();
				addressEntity.setAddress(person.getAddressEntity().getAddress());
				addressEntity.setCity(person.getAddressEntity().getCity());
				addressEntity.setZip(person.getAddressEntity().getZip());
				addressEntity.setStation(0);
				person.setAddressEntity(addressEntity);
				personRepository.save(person);
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e.getStackTrace(), e);
		}
	}
}
