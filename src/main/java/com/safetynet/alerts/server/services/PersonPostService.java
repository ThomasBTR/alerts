package com.safetynet.alerts.server.services;

import com.safetynet.alerts.server.constants.EActionsProceedConstants;
import com.safetynet.alerts.server.database.entities.PersonEntity;
import com.safetynet.alerts.server.database.repositories.PersonRepository;
import com.safetynet.alerts.server.mapping.IPersonMapper;
import io.swagger.model.PersonReq;
import io.swagger.model.PersonsReq;
import io.swagger.model.PersonsRsp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * The type Person post service.
 */
@Service
public class PersonPostService {

	/**
	 * The Person repository.
	 */
	@Autowired
	public PersonRepository personRepository;

	/**
	 * Instantiates a new Person post service.
	 */
	PersonPostService() {
	}

	/**
	 * Instantiates a new Person post service.
	 *
	 * @param personRepository the person repository
	 */
	public PersonPostService(final PersonRepository personRepository) {
		this.personRepository = personRepository;
	}

	/**
	 * The constant logger.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(PersonPostService.class);

	/**
	 * Add persons persons rsp.
	 *
	 * @param personsReq the persons req
	 * @return the persons rsp
	 */
	public PersonsRsp addPersons(final PersonsReq personsReq) {
		LOGGER.debug(EActionsProceedConstants.ADDING_MULTIPLE_PERSONS_SUCCESS.getValue());
		try {
			PersonsRsp personsRsp = new PersonsRsp();

			for (PersonReq person
					:
					personsReq.getPersons()) {
				PersonEntity personEntity = IPersonMapper.INSTANCE.personReqToPersonEntity(person);
				personRepository.save(personEntity);
				LOGGER.debug(EActionsProceedConstants.ADDING_PERSON_SUCCESS.getValue(), person.getFirstName(), person.getLastName());
				personsRsp.addPersonsItem(IPersonMapper.INSTANCE.personEntityToPersonRsp(personEntity));
			}
			return personsRsp;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e.getStackTrace(), e);
			return null;
		}
	}

	/**
	 * Add person.
	 *
	 * @param personReq the person req
	 */
	public void addPerson(final PersonReq personReq) {
		LOGGER.debug(EActionsProceedConstants.ADDING_PERSON_START.getValue(), personReq.getFirstName(), personReq.getLastName());
		try {
			PersonEntity personEntity = IPersonMapper.INSTANCE.personReqToPersonEntity(personReq);
			personRepository.save(personEntity);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e.getStackTrace(), e);
		}
	}

	/**
	 * Update person.
	 *
	 * @param body the body
	 */
	public void updatePerson(final PersonReq body) {
		LOGGER.debug(EActionsProceedConstants.UPDATING_PERSON_START.getValue(), body.getFirstName(), body.getLastName());
		try {
			PersonEntity personEntity = IPersonMapper.INSTANCE.personReqToPersonEntity(body);
			personRepository.save(personEntity);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e.getStackTrace(), e);
		}
	}

	/**
	 * Delete person.
	 *
	 * @param firstName the first name
	 * @param lastName  the last name
	 */
	public void deletePerson(final String firstName, final String lastName) {
		LOGGER.debug(EActionsProceedConstants.DELETING_PERSON_START.getValue(), firstName, lastName);
		try {
			personRepository.delete(personRepository.findPersonEntityByNameEntityLike(firstName, lastName));
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e.getStackTrace(), e);
		}
	}


}
