package com.safetynet.alerts.server.services;

import com.safetynet.alerts.server.constants.EActionsProceedConstants;
import com.safetynet.alerts.server.constants.ENumericalConstants;
import com.safetynet.alerts.server.constants.EObjectConstants;
import com.safetynet.alerts.server.constants.EStatusConstants;
import com.safetynet.alerts.server.database.entities.PersonEntity;
import com.safetynet.alerts.server.database.repositories.PersonRepository;
import io.swagger.model.Firestation;
import io.swagger.model.PersonReq;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Firestation get services.
 */
@Service
public class FirestationGetServices {

	/**
	 * The Person repository.
	 */
	@Autowired
	public PersonRepository personRepository;

	/**
	 * Logger of FirestationGetServices class.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(FirestationGetServices.class);

	/**
	 * Instantiates a new Firestation get services.
	 */
	FirestationGetServices() {
	}

	/**
	 * Instantiates a new Firestation get services.
	 *
	 * @param personRepository the person repository
	 */
	public FirestationGetServices(final PersonRepository personRepository) {
		this.personRepository = personRepository;
	}

	/**
	 * Gets persons infos from firestation id.
	 *
	 * @param stationNumber the station number
	 * @return the persons infos from firestation id
	 */
	public Firestation getPersonsInfosFromFirestationID(final Integer stationNumber) {
		LOGGER.debug(EActionsProceedConstants.FIRESTATIONINFOS_FROMID_START.getValue(), stationNumber);

		List<PersonEntity> personEntities = personRepository.findPersonEntitiesByAddressEntityStation(stationNumber);
		LOGGER.debug(EStatusConstants.DATA_RECEIVED_LIST.getValue(), EObjectConstants.PERSON.getObject(), personEntities.size());
		int adultCount = 0;
		int childCount = 0;
		List<PersonReq> persons = new ArrayList<>();

		for (final PersonEntity personEntity : personEntities) {
			if (personEntity.getBirthdate().isAfter(LocalDate.now().minusYears(Integer.parseInt(ENumericalConstants.LEGAL_ADULT_AGE.getValue())))) {
				++childCount;
			} else {
				++adultCount;
			}
			PersonReq person = new PersonReq();
			person.setAddress(personEntity.getAddressEntity().getAddress());
			person.setCity(personEntity.getAddressEntity().getCity());
			person.setZip(personEntity.getAddressEntity().getZip());
			person.setFirstName(personEntity.getNameEntity().getFirstName());
			person.setLastName(personEntity.getNameEntity().getLastName());
			person.setEmail(personEntity.getEmail());
			person.setPhone(personEntity.getPhone());
			persons.add(person);
		}

		Firestation firestationResponseBody = new Firestation();

		firestationResponseBody.setAdultCount(adultCount);
		firestationResponseBody.setChildCount(childCount);
		firestationResponseBody.setPersons(persons);

		LOGGER.info(EActionsProceedConstants.FIRESTATIONINFOS_FROMID_SUCCESS.getValue(), stationNumber, persons.size(), childCount, adultCount);

		return firestationResponseBody;
	}


}
