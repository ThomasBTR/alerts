package com.safetynet.alerts.server.services;

import com.safetynet.alerts.server.constants.EActionsProceedConstants;
import com.safetynet.alerts.server.constants.ENumericalConstants;
import com.safetynet.alerts.server.constants.EObjectConstants;
import com.safetynet.alerts.server.constants.EStatusConstants;
import com.safetynet.alerts.server.database.entities.AllergeneEntity;
import com.safetynet.alerts.server.database.entities.MedicationEntity;
import com.safetynet.alerts.server.database.entities.PersonEntity;
import com.safetynet.alerts.server.database.repositories.PersonRepository;
import io.swagger.model.Allergies;
import io.swagger.model.Child;
import io.swagger.model.ChildAlert;
import io.swagger.model.CityMailingList;
import io.swagger.model.Fire;
import io.swagger.model.FloodStation;
import io.swagger.model.FloodStation1;
import io.swagger.model.FloodstationPersonsInfo;
import io.swagger.model.Medications;
import io.swagger.model.MedicationsMedicineEntity;
import io.swagger.model.PersonInfo;
import io.swagger.model.PersonInfo1;
import io.swagger.model.Personinfos;
import io.swagger.model.Phone;
import io.swagger.model.PhoneAlert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Person get service.
 */
@Service
public class PersonGetService {

	/**
	 * The Person repository.
	 */
	@Autowired
	public PersonRepository personRepository;

	/**
	 * The constant logger.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(PersonGetService.class);

	/**
	 * Instantiates a new Person get service.
	 */
	public PersonGetService() {
	}

	/**
	 * Instantiates a new Person get service.
	 *
	 * @param personRepository the person repository
	 */
	public PersonGetService(final PersonRepository personRepository) {
		this.personRepository = personRepository;
	}

	/**
	 * Gets children info from address.
	 *
	 * @param address the address
	 * @return the children info from address
	 */
	public ChildAlert getChildrenInfoFromAddress(final String address) {
		LOGGER.debug(EActionsProceedConstants.CHILDALERT_START.getValue(), address);
		try {
			ChildAlert childAlert = new ChildAlert();

			List<PersonEntity> personEntityList = personRepository.findPersonEntityByAddressEntityEquals(address);
			LOGGER.debug(EStatusConstants.DATA_RECEIVED_LIST.getValue(), EObjectConstants.PERSON, personEntityList.size());
			List<PersonInfo1> personInfo1List = new ArrayList<>();
			for (PersonEntity person
					:
					personEntityList) {
				if (person.getBirthdate().isAfter(LocalDate.now().minusYears(Integer.parseInt(ENumericalConstants.LEGAL_ADULT_AGE.getValue())))) {
					Child child = new Child();
					child.setFirstName(person.getNameEntity().getFirstName());
					child.setLastName(person.getNameEntity().getLastName());
					child.setAge(String.valueOf(getAge(person.getBirthdate())));
					childAlert.addChildItem(child);
				} else {
					PersonInfo1 personInfo1 = new PersonInfo1();
					personInfo1.setFirstName(person.getNameEntity().getFirstName());
					personInfo1.setLastName(person.getNameEntity().getLastName());
					personInfo1.setPhone(person.getPhone());
					personInfo1List.add(personInfo1);
				}
			}

			for (Child child
					:
					childAlert.getChild()) {
				child.setPersons(personInfo1List);
			}


			return childAlert;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e.getStackTrace(), e);
			return null;
		}
	}

	/**
	 * Gets age.
	 *
	 * @param birthDate the birth date
	 * @return the age
	 */
	public int getAge(final LocalDate birthDate) {
		if (birthDate != null) {
			return Period.between(birthDate, LocalDate.now()).getYears();
		} else {
			return 0;
		}
	}

	/**
	 * Gets phone alert.
	 *
	 * @param station the station
	 * @return the phone alert
	 */
	public PhoneAlert getPhoneAlert(final int station) {
		LOGGER.debug(EActionsProceedConstants.PHONEALERT_START.getValue(), station);

		PhoneAlert phoneAlert = new PhoneAlert();

		List<PersonEntity> personEntityList = personRepository.findPersonEntitiesByAddressEntityContainingSpecificStation(station);
		LOGGER.debug(EStatusConstants.DATA_RECEIVED_LIST.getValue(), EObjectConstants.PERSON, personEntityList.size());


		for (PersonEntity person
				:
				personEntityList) {
			phoneAlert.addPhonesItem(person.getPhone());
		}

		return phoneAlert;
	}

	/**
	 * Gets fire body.
	 *
	 * @param address the address
	 * @return the fire body
	 */
	public Fire getFireBody(final String address) {
		LOGGER.debug(EActionsProceedConstants.FIREBODY_START.getValue(), address);

		Fire fire = new Fire();

		List<PersonEntity> personEntityList = personRepository.findPersonEntityByAddressEntityEquals(address);
		LOGGER.debug(EStatusConstants.DATA_RECEIVED_LIST.getValue(), EObjectConstants.PERSON, personEntityList.size());


		Integer adultCount = 0;
		Integer childCount = 0;

		for (PersonEntity person
				:
				personEntityList) {
			Phone phone = new Phone();
			phone.setPhone(person.getPhone());
			fire.addPhonesItem(phone);

			if (person.getBirthdate().isAfter(LocalDate.now().minusYears(Integer.parseInt(ENumericalConstants.LEGAL_ADULT_AGE.getValue())))) {
				++childCount;
			} else {
				++adultCount;
			}

		}

		fire.setAdultCount(adultCount);
		fire.setChildCount(childCount);

		return fire;

	}

	/**
	 * Gets flood station.
	 *
	 * @param firestationList the firestation list
	 * @return the flood station
	 */
	public FloodStation getFloodStation(final List<Integer> firestationList) {
		FloodStation floodStation = new FloodStation();

		for (Integer station
				:
				firestationList) {
			List<PersonEntity> personEntityList = personRepository.findPersonEntitiesByAddressEntityContainingSpecificStation(station);
			LOGGER.debug(EStatusConstants.DATA_RECEIVED_LIST.getValue(), EObjectConstants.PERSON, personEntityList.size());


			personEntityList.sort(PersonEntity.Comparators.ADDRESS);

			FloodStation1 floodStation1 = null;
			String address = null;
			for (PersonEntity person
					:
					personEntityList) {
				if (!person.getAddressEntity().getAddress().equals(address)) {
					if (null != floodStation1) {
						floodStation.addAddressesItem(floodStation1);
					}
					floodStation1 = new FloodStation1();
					address = person.getAddressEntity().getAddress();
					floodStation1.setAddress(person.getAddressEntity().getAddress());
					floodStation1.setCity(person.getAddressEntity().getCity());
					floodStation1.setZip(person.getAddressEntity().getZip());
					floodStation1.addPersonsInfoItem(createFloodstationPerson(person));
				} else {
					floodStation1.addPersonsInfoItem(createFloodstationPerson(person));
				}
			}
			floodStation.addAddressesItem(floodStation1);
		}


		return floodStation;
	}

	/**
	 * Gets persons infos.
	 *
	 * @param firstName the first name
	 * @param lastName  the last name
	 * @return the persons infos
	 */
	public PersonInfo getPersonsInfos(final String firstName, final String lastName) {
		LOGGER.debug(EActionsProceedConstants.PERSONINFO_START.getValue(), firstName, lastName);
		PersonEntity personEntity = personRepository.findPersonEntityByNameEntityLike(firstName, lastName);
		LOGGER.debug(EStatusConstants.DATA_RECEIVED_SOLO.getValue(), EObjectConstants.PERSON);


		Personinfos personinfos = new Personinfos();

		personinfos.setAge(getAge(personEntity.getBirthdate()));
		personinfos.setAllergies(getAllergiesInfo(personEntity.getAllergies()));
		personinfos.setMedications(getpersonsInfoMedications(personEntity.getMedications()));
		personinfos.setFirstName(firstName);
		personinfos.setLastName(lastName);
		personinfos.setEmail(personEntity.getEmail());

		PersonInfo personInfo = new PersonInfo();
		personInfo.addPersonsinfosItem(personinfos);

		return personInfo;
	}


	/**
	 * Create floodstation person floodstation persons info.
	 *
	 * @param person the person
	 * @return the floodstation persons info
	 */
	private FloodstationPersonsInfo createFloodstationPerson(final PersonEntity person) {
		FloodstationPersonsInfo personsInfo = new FloodstationPersonsInfo();
		personsInfo.setFirstName(person.getNameEntity().getFirstName());
		personsInfo.setLastName(person.getNameEntity().getLastName());
		personsInfo.setPhone(person.getPhone());
		personsInfo.setAllergies(getAllergiesInfo(person.getAllergies()));
		personsInfo.setMedications(getpersonsInfoMedications(person.getMedications()));

		return personsInfo;
	}

	/**
	 * Gets info medications.
	 *
	 * @param medications the medications
	 * @return the info medications
	 */
	private List<Medications> getpersonsInfoMedications(final List<MedicationEntity> medications) {
		List<Medications> personsInfoMedications = new ArrayList<>();
		for (MedicationEntity medicationEntity
				:
				medications) {
			Medications medicationsInfos = new Medications();
			medicationsInfos.setId(medicationEntity.getId());
			medicationsInfos.setDosage(medicationEntity.getDosage());

			MedicationsMedicineEntity medicationsMedicineEntity = new MedicationsMedicineEntity();
			medicationsMedicineEntity.setId(medicationEntity.getMedicineEntity().getId());
			medicationsMedicineEntity.setMedicineName(medicationEntity.getMedicineEntity().getMedecineName());

			medicationsInfos.setMedicineEntity(medicationsMedicineEntity);
			personsInfoMedications.add(medicationsInfos);
		}
		return personsInfoMedications;
	}

	/**
	 * Gets allergies info.
	 *
	 * @param allergies the allergies
	 * @return the allergies info
	 */
	private List<Allergies> getAllergiesInfo(final List<AllergeneEntity> allergies) {
		List<Allergies> personsInfoAllergies = new ArrayList<>();
		for (AllergeneEntity allergeneEntity
				:
				allergies) {
			Allergies allergiesinfo = new Allergies();
			allergiesinfo.setAllergene(allergeneEntity.getAllergene());
			allergiesinfo.setId(allergeneEntity.getId());
			personsInfoAllergies.add(allergiesinfo);
		}

		return personsInfoAllergies;
	}


	/**
	 * Gets city mailing list.
	 *
	 * @param city the city
	 * @return the city mailing list
	 */
	public CityMailingList getCityMailingList(final String city) {
		LOGGER.debug(EActionsProceedConstants.COMMUNITYEMAIL_START.getValue(), city);
		CityMailingList cityMailingList = new CityMailingList();

		List<PersonEntity> personEntities = personRepository.findPersonEntitiesByAddressEntityContainingCity(city);
		LOGGER.debug(EStatusConstants.DATA_RECEIVED_LIST.getValue(), EObjectConstants.PERSON, personEntities.size());


		for (PersonEntity person
				:
				personEntities) {
			cityMailingList.addEmailsItem(person.getEmail());
		}

		return cityMailingList;
	}
}
