package com.safetynet.alerts.server.constants;

/**
 * The enum E object constants.
 */
public enum EObjectConstants {
	/**
	 * Station e object constants.
	 */
	STATION("Station"),
	/**
	 * Firestation e object constants.
	 */
	FIRESTATION("Firestation"),
	/**
	 * Person e object constants.
	 */
	PERSON("Person");

	/**
	 * The Object.
	 */
	private final String object;

	/**
	 * Instantiates a new E object constants.
	 *
	 * @param object the object
	 */
	EObjectConstants(final String object) {
		this.object = object;
	}

	/**
	 * Get object string.
	 *
	 * @return the string
	 */
	public String getObject() {
		return object;
	}

}
