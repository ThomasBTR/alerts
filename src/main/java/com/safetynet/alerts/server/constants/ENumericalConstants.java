package com.safetynet.alerts.server.constants;

/**
 * The enum E numerical constants.
 */
public enum ENumericalConstants {
	/**
	 * Legal adult age e numerical constants.
	 */
	LEGAL_ADULT_AGE("18");

	/**
	 * Value of the Numerical Constants.
	 */
	private final String value;

	/**
	 * Instantiates a new E numerical constants.
	 *
	 * @param value the value
	 */
	ENumericalConstants(final String value) {
		this.value = value;
	}

	/**
	 * Gets value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
}
