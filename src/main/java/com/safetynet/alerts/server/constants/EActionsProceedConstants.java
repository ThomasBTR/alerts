package com.safetynet.alerts.server.constants;

/**
 * The enum E actions proceed constants.
 */
public enum EActionsProceedConstants {
	/**
	 * The Firestationinfos fromid start.
	 */
	FIRESTATIONINFOS_FROMID_START("started process to get Persons Infos linked to Firestation #{}"),
	/**
	 * The Firestationinfos fromid success.
	 */
	FIRESTATIONINFOS_FROMID_SUCCESS("Persons infos find from firestation #{}, with {} persons, {} childs and {} adults"),
	/**
	 * The Firestationinfos fromid error.
	 */
	FIRESTATIONINFOS_FROMID_ERROR("Error processing FirestationInfos."),
	/**
	 * The Personinfo start.
	 */
	PERSONINFO_START("started process to get PersonsInfos linked to {} {}"),
	/**
	 * The Personinfo success.
	 */
	PERSONINFO_SUCCESS("Persons infos retreived for {} {}."),
	/**
	 * The Personinfo error.
	 */
	PERSONINFO_ERROR("Error processing PersonInfo."),
	/**
	 * The Communityemail start.
	 */
	COMMUNITYEMAIL_START("started process to get CommunityEmail object linked to the following city : {}"),
	/**
	 * The Communityemail success.
	 */
	COMMUNITYEMAIL_SUCCESS("Retreived CommunityMail object linked to the following city : {}."),
	/**
	 * The Communityemail error.
	 */
	COMMUNITYEMAIL_ERROR("Error processing CommunityMail."),
	/**
	 * The Childalert start.
	 */
	CHILDALERT_START("started process to get ChildAlert Object for the following address :{}"),
	/**
	 * The Childalert success.
	 */
	CHILDALERT_SUCCESS("ChildAlert object created for the following address : {}"),
	/**
	 * The Childalert error.
	 */
	CHILDALERT_ERROR("Error processing ChildAlert Object."),
	/**
	 * The Phonealert start.
	 */
	PHONEALERT_START("started process to get PhoneAlert Object for the following Firestation #{}"),
	/**
	 * The Phonealert success.
	 */
	PHONEALERT_SUCCESS("PhoneAlert object created for the following Firestation #{}"),
	/**
	 * The Phonealert error.
	 */
	PHONEALERT_ERROR("Error processing PhoneAlert Object."),
	/**
	 * The Firebody start.
	 */
	FIREBODY_START("started process to get Fire Object for the following address :{}"),
	/**
	 * The Firebody success.
	 */
	FIREBODY_SUCCESS("Fire object created for the following address :{}"),
	/**
	 * The Firebody error.
	 */
	FIREBODY_ERROR("Error processing Fire Object."),
	/**
	 * The Adding firestation start.
	 */
	ADDING_FIRESTATION_START("Started process to add Firestation #{} to the following address : {}"),
	/**
	 * The Adding firestation success.
	 */
	ADDING_FIRESTATION_SUCCESS("Firestation #{} added in database the following address : {}"),
	/**
	 * The Adding firestation error.
	 */
	ADDING_FIRESTATION_ERROR("Error processing Firestation - add."),
	/**
	 * The Updating firestation start.
	 */
	UPDATING_FIRESTATION_START("Started process to update Firestation #{} to the following address : {}"),
	/**
	 * The Updating firestation success.
	 */
	UPDATING_FIRESTATION_SUCCESS("Firestation #{} updated in database the following address : {}"),
	/**
	 * The Updating firestation error.
	 */
	UPDATING_FIRESTATION_ERROR("Error processing Firestation - update."),
	/**
	 * The Deleting firestation start.
	 */
	DELETING_FIRESTATION_START("Started process to delete Firestation #{} to the following address : {}"),
	/**
	 * The Deleting firestation success.
	 */
	DELETING_FIRESTATION_SUCCESS("Firestation #{} deleted in database the following address : {}"),
	/**
	 * The Deleting firestation error.
	 */
	DELETING_FIRESTATION_ERROR("Error processing Firestation - delete."),
	/**
	 * The Adding person start.
	 */
	ADDING_PERSON_START("Started process to add Person named {} {} in the database."),
	/**
	 * The Adding person success.
	 */
	ADDING_PERSON_SUCCESS("Succeed to add {} {} in the database."),
	/**
	 * The Adding person error.
	 */
	ADDING_PERSON_ERROR("Error processing Person - add."),
	/**
	 * The Updating person start.
	 */
	UPDATING_PERSON_START("Started process to update Person named {} {} in the database."),
	/**
	 * The Updating person success.
	 */
	UPDATING_PERSON_SUCCESS("Succeed to update {} {} in the database."),
	/**
	 * The Updating person error.
	 */
	UPDATING_PERSON_ERROR("Error processing Person - update."),
	/**
	 * The Deleting person start.
	 */
	DELETING_PERSON_START("Started process to delete Person named {} {} in the database."),
	/**
	 * The Deleting person success.
	 */
	DELETING_PERSON_SUCCESS("Succeed to delete {} {} in the database."),
	/**
	 * The Deleting person error.
	 */
	DELETING_PERSON_ERROR("Error processing Person - delete."),
	/**
	 * The Adding medical record start.
	 */
	ADDING_MEDICAL_RECORD_START("Started process to add MEDICAL_RECORD for {} {} in the database."),
	/**
	 * The Adding medical record success.
	 */
	ADDING_MEDICAL_RECORD_SUCCESS("Succeed to add MEDICAL_RECORD {} {} in the database."),
	/**
	 * The Adding medical record error.
	 */
	ADDING_MEDICAL_RECORD_ERROR("Error processing MEDICAL_RECORD - add."),
	/**
	 * The Updating medical record start.
	 */
	UPDATING_MEDICAL_RECORD_START("Started process to update MEDICAL_RECORD for {} {} in the database."),
	/**
	 * The Updating medical record success.
	 */
	UPDATING_MEDICAL_RECORD_SUCCESS("Succeed to update MEDICAL_RECORD for {} {} in the database."),
	/**
	 * The Updating medical record error.
	 */
	UPDATING_MEDICAL_RECORD_ERROR("Error processing MEDICAL_RECORD - update."),
	/**
	 * The Deleting medical record start.
	 */
	DELETING_MEDICAL_RECORD_START("Started process to MEDICAL_RECORD for {} {} in the database."),
	/**
	 * The Deleting medical record success.
	 */
	DELETING_MEDICAL_RECORD_SUCCESS("Succeed to MEDICAL_RECORD for {} {} in the database."),
	/**
	 * The Deleting medical record error.
	 */
	DELETING_MEDICAL_RECORD_ERROR("Error processing MEDICAL_RECORD - delete."),
	/**
	 * The Adding multiple persons start.
	 */
	ADDING_MULTIPLE_PERSONS_START("Started process to add multiple Persons in the database."),
	/**
	 * The Adding multiple persons success.
	 */
	ADDING_MULTIPLE_PERSONS_SUCCESS("Succeed to add multiple Persons in the database."),
	/**
	 * The Adding multiple persons error.
	 */
	ADDING_MULTIPLE_PERSONS_ERROR("Error processing Person - add - multiple persons."),
	/**
	 * The Adding multiple medical records start.
	 */
	ADDING_MULTIPLE_MEDICAL_RECORDS_START("Started process to add multiple MEDICAL_RECORDs in the database."),
	/**
	 * The Adding multiple medical records success.
	 */
	ADDING_MULTIPLE_MEDICAL_RECORDS_SUCCESS("Succeed to add multiple MEDICAL_RECORDs in the database."),
	/**
	 * The Adding multiple medical records error.
	 */
	ADDING_MULTIPLE_MEDICAL_RECORDS_ERROR("Error processing Person - add - multiple MEDICAL_RECORDs."),
	/**
	 * The Adding multiple firestations start.
	 */
	ADDING_MULTIPLE_FIRESTATIONS_START("Started process to add multiple Firestations in the database."),
	/**
	 * The Adding multiple firestations success.
	 */
	ADDING_MULTIPLE_FIRESTATIONS_SUCCESS("Succeed to add multiple Firestations in the database."),
	/**
	 * The Adding multiple firestations error.
	 */
	ADDING_MULTIPLE_FIRESTATIONS_ERROR("Error processing Person - add - multiple Firestations.");


	/**
	 * The Value.
	 */
	private final String value;

	/**
	 * Instantiates a new E actions proceed constants.
	 *
	 * @param value the value
	 */
	EActionsProceedConstants(final String value) {
		this.value = value;
	}

	/**
	 * Get value string.
	 *
	 * @return the string
	 */
	public String getValue() {
		return value;
	}

}
