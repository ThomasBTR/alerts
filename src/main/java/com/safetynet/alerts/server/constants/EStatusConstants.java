package com.safetynet.alerts.server.constants;

/**
 * The enum E status constants.
 */
public enum EStatusConstants {


	/**
	 * The Added.
	 */
	ADDED("%s added to the database"),
	/**
	 * The Updated.
	 */
	UPDATED("%s updated in database"),
	/**
	 * The Deleted.
	 */
	DELETED("%s deleted form the database"),
	/**
	 * The Data received list.
	 */
	DATA_RECEIVED_LIST("{} data received form the database with {} entity(ies)."),
	/**
	 * The Data received solo.
	 */
	DATA_RECEIVED_SOLO("{} entity received form the database.");


	/**
	 * The Value.
	 */
	private final String value;

	/**
	 * Instantiates a new E status constants.
	 *
	 * @param value the value
	 */
	EStatusConstants(final String value) {
		this.value = value;
	}

	/**
	 * Gets value.
	 *
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
}
