package com.safetynet.alerts.server.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.safetynet.alerts.server.constants.EActionsProceedConstants;
import com.safetynet.alerts.server.services.FirestationGetServices;
import com.safetynet.alerts.server.services.FirestationPostServices;
import io.swagger.api.AddFirestationsApi;
import io.swagger.api.StationNumberApi;
import io.swagger.model.AddressesRsp;
import io.swagger.model.Firestation;
import io.swagger.model.Firestations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * The type Firestation controller.
 */
@RestController
public class FirestationController implements AddFirestationsApi, StationNumberApi {
	/**
	 * The Firestation get services.
	 */
	@Autowired
	private FirestationGetServices firestationGetServices;

	/**
	 * The Firestation post services.
	 */
	@Autowired
	private FirestationPostServices firestationPostServices;

	/**
	 * Logger of FirestationController Class.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(FirestationController.class);


	/**
	 * Instantiates a new Firestation controller.
	 */
	FirestationController() {
	}

	/**
	 * Instantiates a new Firestation controller.
	 *
	 * @param firestationPostServices the firestation post services
	 * @param firestationGetServices  the firestation get services
	 */
	public FirestationController(final FirestationPostServices firestationPostServices, final FirestationGetServices firestationGetServices) {
		this.firestationGetServices = firestationGetServices;
		this.firestationPostServices = firestationPostServices;
	}

	/**
	 * GetObjectMapper.
	 *
	 * @return a mappedObject.
	 */
	@Override
	public Optional<ObjectMapper> getObjectMapper() {
		return AddFirestationsApi.super.getObjectMapper();
	}

	/**
	 * GetRequest.
	 *
	 * @return a request.
	 */
	@Override
	public Optional<HttpServletRequest> getRequest() {
		return AddFirestationsApi.super.getRequest();
	}

	/**
	 * GetAcceptHeader.
	 *
	 * @return The Accepted Headers.
	 */
	@Override
	public Optional<String> getAcceptHeader() {
		return AddFirestationsApi.super.getAcceptHeader();
	}

	/**
	 * POST a list of Firestations and return the Address list modified by the POST action.
	 *
	 * @param body Firestation list
	 * @return The adresses modified
	 */
	@Override
	public ResponseEntity<AddressesRsp> addFirestationToDatabase(final Firestations body) {
		ResponseEntity<AddressesRsp> response = null;

		try {
			response = ResponseEntity.ok(firestationPostServices.addFirestations(body));
			LOGGER.info(EActionsProceedConstants.ADDING_MULTIPLE_FIRESTATIONS_SUCCESS.getValue());
		} catch (Exception e) {
			LOGGER.error(EActionsProceedConstants.ADDING_MULTIPLE_FIRESTATIONS_ERROR.getValue(), e);
		}

		return response;
	}

	/**
	 * GET persons infos from a Firestation ID.
	 *
	 * @param stationNumber the station number from where the user want info
	 * @return A Firestation Object with the persons info.
	 */
	@Override
	public ResponseEntity<Firestation> getPersonsInfosFromFirestationID(final Integer stationNumber) {
		ResponseEntity<Firestation> response = null;

		try {
			response = ResponseEntity.ok(firestationGetServices.getPersonsInfosFromFirestationID(stationNumber));
		} catch (Exception e) {
			LOGGER.error(EActionsProceedConstants.FIRESTATIONINFOS_FROMID_ERROR.getValue(), e);
		}

		return response;
	}

	/***
	 * ADD a firestation mapping.
	 * @param address the address to map
	 * @param stationNumber the station number to map
	 * @return a created status.
	 */
	@Override
	public ResponseEntity<Void> addFirestationMappingToSpecifiedAddress(final String address, final Integer stationNumber) {
		try {
			firestationPostServices.addFirestationMappingToASpecifiedAddress(stationNumber, address);
			LOGGER.info(EActionsProceedConstants.ADDING_FIRESTATION_SUCCESS.getValue(), stationNumber, address);
		} catch (Exception e) {
			LOGGER.error(EActionsProceedConstants.ADDING_FIRESTATION_ERROR.getValue(), e);
		}
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	/**
	 * UPDATE a firestation mapping.
	 *
	 * @param address       the address for the mapping
	 * @param stationNumber the station number mapped
	 * @return a created status.
	 */
	@Override
	public ResponseEntity<Void> updateFirestationMappingToSpecifiedAddress(final String address, final Integer stationNumber) {
		try {
			firestationPostServices.updateFirestationMappingToASpecifiedAddress(stationNumber, address);
			LOGGER.info(EActionsProceedConstants.UPDATING_FIRESTATION_SUCCESS.getValue(), stationNumber, address);
		} catch (Exception e) {
			LOGGER.error(EActionsProceedConstants.UPDATING_FIRESTATION_ERROR.getValue(), e);
		}
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	/**
	 * DELETE a firestation mapping.
	 *
	 * @param address       the address of the mapping
	 * @param stationNumber the station mapped
	 * @return a status OK No Content.
	 */
	@Override
	public ResponseEntity<Void> deleteFirestationMappingToSpecifiedAddress(final String address, final Integer stationNumber) {
		try {
			firestationPostServices.deleteFirestationMappingToASpecifiedAddress(stationNumber, address);
			LOGGER.info(EActionsProceedConstants.DELETING_FIRESTATION_SUCCESS.getValue(), stationNumber, address);
		} catch (Exception e) {
			LOGGER.error(EActionsProceedConstants.DELETING_FIRESTATION_ERROR.getValue(), e);
		}
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
