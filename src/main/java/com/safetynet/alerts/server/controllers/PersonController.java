package com.safetynet.alerts.server.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.safetynet.alerts.server.constants.EActionsProceedConstants;
import com.safetynet.alerts.server.services.PersonGetService;
import com.safetynet.alerts.server.services.PersonPostService;
import io.swagger.api.AddPersonsApi;
import io.swagger.api.ChildAlertApi;
import io.swagger.api.CommunityEmailApi;
import io.swagger.api.FireApi;
import io.swagger.api.FloodStationApi;
import io.swagger.api.PersonApi;
import io.swagger.api.PersonInfoApi;
import io.swagger.api.PhoneAlertApi;
import io.swagger.model.ChildAlert;
import io.swagger.model.CityMailingList;
import io.swagger.model.Fire;
import io.swagger.model.FloodStation;
import io.swagger.model.PersonInfo;
import io.swagger.model.PersonReq;
import io.swagger.model.PersonsReq;
import io.swagger.model.PersonsRsp;
import io.swagger.model.PhoneAlert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

/**
 * The type Person controller.
 */
@RestController
public class PersonController implements AddPersonsApi, ChildAlertApi, PhoneAlertApi, FireApi, FloodStationApi, PersonInfoApi, CommunityEmailApi, PersonApi {

	/**
	 * The Person post service.
	 */
	@Autowired
	PersonPostService personPostService;

	/**
	 * The Person get service.
	 */
	@Autowired
	PersonGetService personGetService;

	/**
	 * Instantiates a new Person controller.
	 *
	 * @param personGetService  the person get service
	 * @param personPostService the person post service
	 */
	public PersonController(final PersonGetService personGetService, final PersonPostService personPostService) {
		this.personGetService = personGetService;
		this.personPostService = personPostService;
	}

	/**
	 * PersonController logger.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(PersonController.class);

	/**
	 * GET All the children Info from a specific address.
	 *
	 * @param address the address.
	 * @return the list of children.
	 */
	@Override
	public ResponseEntity<ChildAlert> getChildrenInfoFromAddress(final String address) {
		ResponseEntity<ChildAlert> response = null;

		try {
			response = ResponseEntity.ok(personGetService.getChildrenInfoFromAddress(address));
			LOGGER.info(EActionsProceedConstants.CHILDALERT_SUCCESS.getValue(), address);
		} catch (Exception e) {
			LOGGER.error(EActionsProceedConstants.PHONEALERT_ERROR.getValue(), e);
		}

		return response;
	}

	/**
	 * get Perons Info from a first and last name.
	 *
	 * @param firstName the first name.
	 * @param lastName  the last name.
	 * @return the person info found.
	 */
	@Override
	public ResponseEntity<PersonInfo> getPersonInfos(final String firstName, final String lastName) {
		ResponseEntity<PersonInfo> response = null;

		try {
			response = ResponseEntity.ok(personGetService.getPersonsInfos(firstName, lastName));
			LOGGER.info(EActionsProceedConstants.PERSONINFO_SUCCESS.getValue(), firstName, lastName);
		} catch (Exception e) {
			LOGGER.error(EActionsProceedConstants.PERSONINFO_ERROR.getValue(), e);
		}

		return response;
	}

	/**
	 * Get a person info list from a list of firestation.
	 *
	 * @param firestation the firestation list.
	 * @return the floodstation object.
	 */
	@Override
	public ResponseEntity<FloodStation> getAllPersonsInfosFromFirestationID(final List<Integer> firestation) {
		ResponseEntity<FloodStation> response = null;

		try {
			response = ResponseEntity.ok(personGetService.getFloodStation(firestation));
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}

		return response;
	}

	/**
	 * Get a Fire object for a specific address.
	 *
	 * @param address the address.
	 * @return the fire object.
	 */
	@Override
	public ResponseEntity<Fire> getPersonsInfosFromAddress(final String address) {
		ResponseEntity<Fire> response = null;

		try {
			response = ResponseEntity.ok(personGetService.getFireBody(address));
			LOGGER.info(EActionsProceedConstants.FIREBODY_SUCCESS.getValue(), address);
		} catch (Exception e) {
			LOGGER.error(EActionsProceedConstants.FIREBODY_ERROR.getValue(), e);
		}

		return response;
	}

	/**
	 * Get a phone Alert object for a specific firestation.
	 *
	 * @param firestation the firestation ID.
	 * @return phoneAlert object.
	 */
	@Override
	public ResponseEntity<PhoneAlert> getPhoneNumbersFromFirestationID(final Integer firestation) {
		ResponseEntity<PhoneAlert> response = null;

		try {
			response = ResponseEntity.ok(personGetService.getPhoneAlert(firestation));
			LOGGER.info(EActionsProceedConstants.PHONEALERT_SUCCESS.getValue(), firestation);

		} catch (Exception e) {
			LOGGER.error(EActionsProceedConstants.PHONEALERT_ERROR.getValue(), e);
		}

		return response;
	}


	/**
	 * GET ObjectMapper.
	 *
	 * @return ObjectMapper object mapper
	 */
	@Override
	public Optional<ObjectMapper> getObjectMapper() {
		return AddPersonsApi.super.getObjectMapper();
	}

	/**
	 * Get Request.
	 *
	 * @return Request. request
	 */
	@Override
	public Optional<HttpServletRequest> getRequest() {
		return AddPersonsApi.super.getRequest();
	}

	/**
	 * Get accept Header.
	 *
	 * @return Accepted Header
	 */
	@Override
	public Optional<String> getAcceptHeader() {
		return AddPersonsApi.super.getAcceptHeader();
	}

	/**
	 * Add Person to the database.
	 *
	 * @param body person object
	 * @return a Created status.
	 */
	@Override
	public ResponseEntity<Void> addPerson(final PersonReq body) {
		try {
			personPostService.addPerson(body);
			LOGGER.info(EActionsProceedConstants.ADDING_PERSON_SUCCESS.getValue(), body.getFirstName(), body.getLastName());
		} catch (Exception e) {
			LOGGER.error(EActionsProceedConstants.ADDING_PERSON_ERROR.getValue(), e);
		}

		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	/**
	 * DELETE a person from the database from first and last name param.
	 *
	 * @param firstName the first name.
	 * @param lastName  the last name.
	 * @return a ok no content status.
	 */
	@Override
	public ResponseEntity<Void> deletePerson(final String firstName, final String lastName) {
		try {
			personPostService.deletePerson(firstName, lastName);
			LOGGER.info(EActionsProceedConstants.DELETING_PERSON_SUCCESS.getValue(), firstName, lastName);
		} catch (Exception e) {
			LOGGER.error(EActionsProceedConstants.DELETING_PERSON_ERROR.getValue(), e);
		}
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	/**
	 * UPDATE a person Object in the database.
	 *
	 * @param firstName the first name.
	 * @param lastName  the last name.
	 * @param body      the person body use to update the person in the database.
	 * @return a Created status.
	 */
	@Override
	public ResponseEntity<Void> updatePerson(final String firstName, final String lastName, final PersonReq body) {
		try {
			personPostService.updatePerson(body);
			LOGGER.info(EActionsProceedConstants.UPDATING_PERSON_SUCCESS.getValue(), firstName, lastName);
		} catch (Exception e) {
			LOGGER.error(EActionsProceedConstants.UPDATING_PERSON_ERROR.getValue(), e);
		}

		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	/**
	 * ADD a list of persons in the database.
	 *
	 * @param body the persons list.
	 * @return The persons object created in the database
	 */
	@Override
	public ResponseEntity<PersonsRsp> addPersonsToDatabase(final PersonsReq body) {
		ResponseEntity<PersonsRsp> response = null;

		try {
			response = ResponseEntity.ok(personPostService.addPersons(body));
			LOGGER.info(EActionsProceedConstants.ADDING_MULTIPLE_PERSONS_SUCCESS.getValue());
		} catch (Exception e) {
			LOGGER.error(EActionsProceedConstants.ADDING_MULTIPLE_PERSONS_ERROR.getValue(), e);
		}

		return response;
	}

	/**
	 * GET a mailing list from a city String.
	 *
	 * @param city the city string.
	 * @return A city Mailing list.
	 */
	@Override
	public ResponseEntity<CityMailingList> getMailingListFromCity(final String city) {
		ResponseEntity<CityMailingList> response = null;

		try {
			response = ResponseEntity.ok(personGetService.getCityMailingList(city));
			LOGGER.info(EActionsProceedConstants.COMMUNITYEMAIL_SUCCESS.getValue(), city);
		} catch (Exception e) {
			LOGGER.error(EActionsProceedConstants.COMMUNITYEMAIL_ERROR.getValue(), e);
		}

		return response;
	}
}
