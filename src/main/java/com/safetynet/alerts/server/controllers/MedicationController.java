package com.safetynet.alerts.server.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.safetynet.alerts.server.constants.EActionsProceedConstants;
import com.safetynet.alerts.server.services.MedicationPostServices;
import io.swagger.api.AddMedicalRecordsApi;
import io.swagger.api.MedicalRecordApi;
import io.swagger.model.MedicalRecord;
import io.swagger.model.Medicalrecords;
import io.swagger.model.PersonsRsp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * The type Medication controller.
 */
@RestController
public class MedicationController implements AddMedicalRecordsApi, MedicalRecordApi {
	/**
	 * The Medications post services.
	 */
	@Autowired
	MedicationPostServices medicationsPostServices;

	/**
	 * MedicationController logger.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(MedicationController.class);

	/**
	 * Instantiates a new Medication controller.
	 */
	MedicationController() {
	}

	/**
	 * Instantiates a new Medication controller.
	 *
	 * @param medicationPostServices the medication post services
	 */
	public MedicationController(final MedicationPostServices medicationPostServices) {
		this.medicationsPostServices = medicationPostServices;
	}

	/**
	 * ObjectMapper.
	 *
	 * @return the object mapper
	 */
	@Override
	public Optional<ObjectMapper> getObjectMapper() {
		return AddMedicalRecordsApi.super.getObjectMapper();
	}

	/**
	 * Request.
	 *
	 * @return request
	 */
	@Override
	public Optional<HttpServletRequest> getRequest() {
		return AddMedicalRecordsApi.super.getRequest();
	}

	/**
	 * Header Accepted.
	 *
	 * @return accept header
	 */
	@Override
	public Optional<String> getAcceptHeader() {
		return AddMedicalRecordsApi.super.getAcceptHeader();
	}

	/**
	 * ADD a Medical Record to the database.
	 *
	 * @param body the medical Record.
	 * @return a created status.
	 */
	@Override
	public ResponseEntity<Void> addMedicalRecord(final MedicalRecord body) {
		try {
			medicationsPostServices.addMedicalRecord(body);
			LOGGER.info(EActionsProceedConstants.ADDING_MEDICAL_RECORD_SUCCESS.getValue(), body.getFirstName(), body.getLastName());
		} catch (Exception e) {
			LOGGER.error(EActionsProceedConstants.ADDING_MEDICAL_RECORD_ERROR.getValue(), e);
		}
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	/**
	 * DELETE a medical Record in database for a specific person.
	 *
	 * @param firstName the first name
	 * @param lastName  the last name
	 * @return a status ok no content.
	 */
	@Override
	public ResponseEntity<Void> deleteMedicalRecord(final String firstName, final String lastName) {
		try {
			medicationsPostServices.deleteMedicalRecord(firstName, lastName);
			LOGGER.info(EActionsProceedConstants.DELETING_MEDICAL_RECORD_SUCCESS.getValue(), firstName, lastName);
		} catch (Exception e) {
			LOGGER.error(EActionsProceedConstants.DELETING_MEDICAL_RECORD_ERROR.getValue(), e);
		}
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	/**
	 * UPDATE a Medical Record in database for a specific person.
	 *
	 * @param firstName the first name.
	 * @param lastName  the last name.
	 * @param body      the body with updated infos.
	 * @return a status Created.
	 */
	@Override
	public ResponseEntity<Void> updateMedicalRecord(final String firstName, final String lastName, final MedicalRecord body) {
		try {
			medicationsPostServices.updateMedicalRecord(firstName, lastName, body);
			LOGGER.info(EActionsProceedConstants.UPDATING_MEDICAL_RECORD_SUCCESS.getValue(), body.getFirstName(), body.getLastName());
		} catch (Exception e) {
			LOGGER.error(EActionsProceedConstants.UPDATING_MEDICAL_RECORD_ERROR.getValue(), e);
		}
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	/**
	 * ADD a list of Medical Records to the database for plural persons.
	 *
	 * @param body the medical records list.
	 * @return the Persons object as available in database.
	 */
	@Override
	public ResponseEntity<PersonsRsp> addMedicalRecordsToDatabase(final Medicalrecords body) {
		ResponseEntity<PersonsRsp> response = null;

		try {
			response = ResponseEntity.ok(medicationsPostServices.addMedicalRecords(body));
			LOGGER.info(EActionsProceedConstants.ADDING_MULTIPLE_MEDICAL_RECORDS_SUCCESS.getValue());
		} catch (Exception e) {
			LOGGER.error(EActionsProceedConstants.ADDING_MULTIPLE_MEDICAL_RECORDS_ERROR.getValue(), e);
		}

		return response;
	}
}
