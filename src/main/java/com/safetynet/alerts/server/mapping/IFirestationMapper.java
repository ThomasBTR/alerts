package com.safetynet.alerts.server.mapping;

import com.safetynet.alerts.server.database.entities.AddressEntity;
import com.safetynet.alerts.server.database.entities.PersonEntity;
import io.swagger.model.AddressRsp;
import io.swagger.model.PersonReq;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * The interface Firestation mapper.
 */
@Mapper
public interface IFirestationMapper {

	/**
	 * The constant INSTANCE.
	 */
	IFirestationMapper INSTANCE = Mappers.getMapper(IFirestationMapper.class);


	/**
	 * Address rsp fill address rsp.
	 *
	 * @param addressEntity the address entity
	 * @return the address rsp
	 */
	AddressRsp addressRspFill(AddressEntity addressEntity);
}
