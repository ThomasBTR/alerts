package com.safetynet.alerts.server.database.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * The type Name entity.
 */
@Getter
@Setter
@Embeddable
public class NameEntity implements Serializable {

	/**
	 * The First name.
	 */
	@Column(name = "firstName")
	private String firstName;
	/**
	 * The Last name.
	 */
	@Column(name = "lastName")
	private String lastName;

	/**
	 * Instantiates a new Name entity.
	 */
	public NameEntity() {
	}

}
