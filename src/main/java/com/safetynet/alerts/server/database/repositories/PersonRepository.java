package com.safetynet.alerts.server.database.repositories;

import com.safetynet.alerts.server.database.entities.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * The interface Person repository.
 */
@Repository
public interface PersonRepository extends JpaRepository<PersonEntity, Integer> {

	/**
	 * Find person entities by address entity station list.
	 *
	 * @param stationNumber the station number
	 * @return the list
	 */
	@Query("SELECT p from PersonEntity p WHERE p.addressEntity.station =:variable")
	List<PersonEntity> findPersonEntitiesByAddressEntityStation(@Param("variable") int stationNumber);

	/**
	 * Find person entity by name entity like person entity.
	 *
	 * @param firstName the first name
	 * @param lastName  the last name
	 * @return the person entity
	 */
	@Query("SELECT p from PersonEntity p WHERE p.nameEntity.firstName =:var1 AND p.nameEntity.lastName =:var2")
	PersonEntity findPersonEntityByNameEntityLike(@Param("var1") String firstName, @Param("var2") String lastName);

	/**
	 * Find person entity by address entity equals list.
	 *
	 * @param address the address
	 * @return the list
	 */
	@Query("SELECT p FROM PersonEntity p WHERE p.addressEntity.address =:variable")
	List<PersonEntity> findPersonEntityByAddressEntityEquals(@Param("variable") String address);

	/**
	 * Find person entities by address entity containing specific station list.
	 *
	 * @param station the station
	 * @return the list
	 */
	@Query("SELECT p FROM PersonEntity p WHERE p.addressEntity.station =:variable")
	List<PersonEntity> findPersonEntitiesByAddressEntityContainingSpecificStation(@Param("variable") int station);

	/**
	 * Find person entities by address entity containing city list.
	 *
	 * @param city the city
	 * @return the list
	 */
	@Query("SELECT p FROM PersonEntity p WHERE p.addressEntity.city =:variable")
	List<PersonEntity> findPersonEntitiesByAddressEntityContainingCity(@Param("variable") String city);
}
