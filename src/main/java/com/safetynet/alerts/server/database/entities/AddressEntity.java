package com.safetynet.alerts.server.database.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Embeddable;
import java.util.Comparator;

/**
 * The type Address entity.
 */
@Getter
@Setter
@Embeddable
public class AddressEntity implements Comparable<AddressEntity> {
	/**
	 * The Address.
	 */
	private String address;
	/**
	 * The City.
	 */
	private String city;
	/**
	 * The Zip.
	 */
	private String zip;
	/**
	 * The Station.
	 */
	private int station;

	/**
	 * Instantiates a new Address entity.
	 */
	public AddressEntity() {

	}

	/**
	 * CompareTo method.
	 *
	 * @param o the o
	 * @return the int with 1 if comparable, or 0 if it isn't.
	 */
	@Override
	public int compareTo(final AddressEntity o) {
		return 0;
	}

	/**
	 * The type Comparators.
	 */
	public static class Comparators {
		/**
		 * The constant ADDRESS.
		 */
		public static final Comparator<AddressEntity> ADDRESS = (AddressEntity person1, AddressEntity person2) -> person1.address.compareTo(person2.address);

	}

}
