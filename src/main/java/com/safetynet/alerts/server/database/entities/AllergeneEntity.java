package com.safetynet.alerts.server.database.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The type Allergene entity.
 */
@Entity
@Getter
@Setter
@Table(name = "allergenes")
public class AllergeneEntity {
	/**
	 * The Id.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	/**
	 * The Allergene.
	 */
	private String allergene;

	/**
	 * Instantiates a new Allergene entity.
	 */
	public AllergeneEntity() {
	}

}
