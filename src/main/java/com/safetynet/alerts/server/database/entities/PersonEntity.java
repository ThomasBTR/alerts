package com.safetynet.alerts.server.database.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;

/**
 * The type Person entity.
 */
@Entity
@Getter
@Setter
@Table(name = "persons")
public class PersonEntity implements Comparable<PersonEntity> {

	/**
	 * The Name entity.
	 */
	@EmbeddedId
	private NameEntity nameEntity;
	/**
	 * The Birthdate.
	 */
	private LocalDate birthdate;
	/**
	 * The Email.
	 */
	private String email;
	/**
	 * The Phone.
	 */
	private String phone;
	/**
	 * The Address entity.
	 */
	@Embedded
	private AddressEntity addressEntity;
	/**
	 * The Allergies.
	 */
	@ManyToMany(cascade = CascadeType.PERSIST)
	@JoinTable(name = "allergies", joinColumns = {
			@JoinColumn(name = "firstName", referencedColumnName = "firstName", insertable = false, updatable = false),
			@JoinColumn(name = "lastName", referencedColumnName = "lastName", insertable = false, updatable = false)
	})
	private List<AllergeneEntity> allergies;
	/**
	 * The Medications.
	 */
	@OneToMany(mappedBy = "personEntity", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<MedicationEntity> medications;

	/**
	 * Instantiates a new Person entity.
	 */
	public PersonEntity() {

	}

	/**
	 * Comparable Object.
	 *
	 * @param o comparaison pool.
	 * @return 0 the comparaison status.
	 */
	@Override
	public int compareTo(final PersonEntity o) {
		return 0;
	}


	/**
	 * The type Comparators.
	 */
	public static class Comparators {
		/**
		 * The constant ADDRESS.
		 */
		public static final Comparator<PersonEntity> ADDRESS = (PersonEntity person1, PersonEntity person2) -> person1.addressEntity.getAddress().compareTo(person2.addressEntity.getAddress());

	}


}
