package com.safetynet.alerts.server.database.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The type Medicine entity.
 */
@Getter
@Setter
@Entity
@Table(name = "medicines")
public class MedicineEntity {

	/**
	 * The Id.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	/**
	 * The Medecine name.
	 */
	private String medecineName;

	/**
	 * Instantiates a new Medicine entity.
	 */
	public MedicineEntity() {
	}
}
