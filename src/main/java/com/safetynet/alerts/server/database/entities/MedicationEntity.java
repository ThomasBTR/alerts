package com.safetynet.alerts.server.database.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * The type Medication entity.
 */
@Entity
@Getter
@Setter
public class MedicationEntity {

	/**
	 * The Id.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	/**
	 * The Person entity.
	 */
	@ManyToOne(cascade = CascadeType.PERSIST)
	private PersonEntity personEntity;
	/**
	 * The Medicine entity.
	 */
	@ManyToOne(cascade = {CascadeType.PERSIST})
	private MedicineEntity medicineEntity;
	/**
	 * The Dosage.
	 */
	private int dosage;

	/**
	 * Instantiates a new Medication entity.
	 */
	public MedicationEntity() {
	}
}




