package com.safetynet.alerts.TU.Controller;

import com.safetynet.alerts.UTHelper;
import com.safetynet.alerts.server.controllers.FirestationController;
import com.safetynet.alerts.server.controllers.PersonController;
import com.safetynet.alerts.server.database.entities.*;
import com.safetynet.alerts.server.database.repositories.PersonRepository;
import com.safetynet.alerts.server.services.FirestationGetServices;
import com.safetynet.alerts.server.services.FirestationPostServices;
import com.safetynet.alerts.server.services.PersonGetService;
import com.safetynet.alerts.server.services.PersonPostService;
import io.swagger.model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class FirestationControllerTests {

	FirestationController firestationController;

	@Mock
	PersonRepository personRepository;

	@Mock
	FirestationPostServices firestationPostServices;

	@Mock
	FirestationGetServices firestationGetServices;

	String address = "1509 Culver St";
	int station = 3;

	ChildAlert childAlert = null;
	PhoneAlert phoneAlert = null;
	FloodStation floodStation = null;

	PersonEntity adult = null;
	PersonEntity child = null;


	List<PersonEntity> personEntityListwithChild;
	List<PersonEntity> personEntityListwithoutChild;

	PersonReq personReq = null;

	@BeforeEach
	void prepare() {
		personEntityListwithChild = new ArrayList<>();
		personEntityListwithoutChild = new ArrayList<>();

		firestationController = new FirestationController(firestationPostServices, firestationGetServices);

		MedicationEntity medicationEntity = new MedicationEntity();

		AddressEntity addressEntity = new AddressEntity();

		List<MedicationEntity> medicationEntities = new ArrayList<>();

		MedicineEntity medicineEntity = new MedicineEntity();

		List<AllergeneEntity> allergeneEntities = new ArrayList<>();

		AllergeneEntity allergeneEntity = new AllergeneEntity();

		String phone = "841-874-6512";


		addressEntity.setAddress(address);
		addressEntity.setStation(1);
		addressEntity.setCity("Culver");
		addressEntity.setZip("97451");

		medicineEntity.setMedecineName("aznol");
		medicineEntity.setId(0);

		medicationEntity.setDosage(350);
		medicationEntity.setMedicineEntity(medicineEntity);
		medicationEntity.setId(0);

		allergeneEntity.setAllergene("nillacilan");
		allergeneEntity.setId(0);
		allergeneEntities.add(allergeneEntity);

		NameEntity adultName = new NameEntity();
		adultName.setFirstName("John");
		adultName.setLastName("Boyd");

		NameEntity childName = new NameEntity();
		childName.setFirstName("Tenley");
		childName.setLastName("Boyd");

		adult = new PersonEntity();
		adult.setBirthdate(LocalDate.of(1984, 3, 6));
		adult.setAddressEntity(addressEntity);
		adult.setPhone(phone);
		adult.setAllergies(allergeneEntities);
		adult.setMedications(medicationEntities);
		adult.setNameEntity(adultName);
		adult.setEmail("jaboyd@email.com");

		child = new PersonEntity();
		child.setMedications(medicationEntities);
		child.setAddressEntity(addressEntity);
		child.setPhone(phone);
		child.setAllergies(allergeneEntities);
		child.setMedications(medicationEntities);
		child.setBirthdate(LocalDate.of(2012, 2, 18));
		child.setNameEntity(childName);
		child.setEmail("tenz@email.com");

		personEntityListwithChild.add(child);
		personEntityListwithChild.add(adult);
		personEntityListwithoutChild.add(adult);

		personReq = new PersonReq();
		personReq.setEmail(adult.getEmail());
		personReq.setPhone(adult.getPhone());
		personReq.setAddress(adult.getAddressEntity().getAddress());
		personReq.setCity(adult.getAddressEntity().getCity());
		personReq.setZip(adult.getAddressEntity().getZip());
		personReq.setFirstName(adult.getNameEntity().getFirstName());
		personReq.setLastName(adult.getNameEntity().getLastName());
	}

	@Tag("Get endpoints")
	@Test
	void Firestation_OK_returnFirestation() {
		// GIVEN
		Firestation firestation = null;
		int station = 3;
		try {
			firestation = UTHelper.stringToObject(UTHelper.readFileAsString("responseBody/Persons/StationNumber_200.json"), Firestation.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		doReturn(firestation).when(firestationGetServices).getPersonsInfosFromFirestationID(station);
		// WHEN
		ResponseEntity<Firestation> response = firestationController.getPersonsInfosFromFirestationID(station);

		// THEN
		verify(firestationGetServices, times(1)).getPersonsInfosFromFirestationID(station);
		assertThat(response).isInstanceOf(ResponseEntity.class);
		assertThat(response.getBody()).isInstanceOf(Firestation.class);
		try {
			assertThat(response.getBody()).isEqualTo(UTHelper.stringToObject(UTHelper.readFileAsString("responseBody/Persons/StationNumber_200.json"), Firestation.class));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Tag("POST endpoints")
	@Test
	void FirestationsAdd_200_ReturnBody() {

		// GIVEN
		Firestations firestations = null;
		AddressesRsp addressesRsp = null;
		try {
			firestations = UTHelper.stringToObject(UTHelper.readFileAsString("requestBody/Firestations/firestations.json"), Firestations.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			addressesRsp = UTHelper.stringToObject(UTHelper.readFileAsString("responseBody/ChildAlert/Addresses.json"), AddressesRsp.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		// WHEN
		ResponseEntity<AddressesRsp> response = firestationController.addFirestationToDatabase(firestations);

		// THEN
		assertThat(response).isInstanceOf(ResponseEntity.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
	}

	@Tag("POST endpoints")
	@Test
	void firestationPost_200_ReturnBody() {
		// GIVEN
		// WHEN
		ResponseEntity<Void> response = firestationController.addFirestationMappingToSpecifiedAddress(address,station);

		// THEN
		assertThat(response).isInstanceOf(ResponseEntity.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
	}

	@Tag("PUT endpoints")
	@Test
	void firestationPut_200_ReturnBody() {
		// GIVEN
		// WHEN
		ResponseEntity<Void> response = firestationController.updateFirestationMappingToSpecifiedAddress(address, station);

		// THEN
		assertThat(response).isInstanceOf(ResponseEntity.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
	}

	@Tag("DELETE endpoints")
	@Test
	void firestationDelete_200_ReturnBody() {
		// GIVEN
		// WHEN
		ResponseEntity<Void> response = firestationController.deleteFirestationMappingToSpecifiedAddress(address, station);

		// THEN
		assertThat(response).isInstanceOf(ResponseEntity.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
	}

}
