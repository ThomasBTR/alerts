Pour builder l'api, il suffit de faire un **mvn clean install**

Puis pour lancer l'api : mvn spring-boot:run

Afin de charger la bdd, il faut alimenter le create.sql avec les données d'entrées.

L'api tape sur l'adresse : http://localhost:9000/

Les actuators sont disponible.

La configuration ce fait via le fichier application.properties dans src/main/ressource.